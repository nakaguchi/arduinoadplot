/*
  Analog input and send via serial port
  2017.06.19 T.Nakaguchi
 */

const int CHANNELS = 6; // 入力チャネル数
const int N = 10;       // サンプル平均回数
const int ADPIN[CHANNELS] = {A0, A1, A2, A3, A4, A5};
unsigned long count = 0;
String zero = "0000000000";

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.begin(115200);
}

void ZeroFillPrint(long val, int len) {
  String buf = String(val, DEC);
  Serial.print(zero.substring(0, len - buf.length()));
  Serial.print(buf);  
}

void ZeroFillPrintHex(long val, int len) {
  String buf = String(val, HEX);
  Serial.print(zero.substring(0, len - buf.length()));
  Serial.print(buf);  
}

void loop() {
  // カウンタ
  ZeroFillPrintHex(count ++, 8);
  Serial.print(",");

  // 各A/D入力値をN回加算
  unsigned long val[CHANNELS] = {0, 0, 0, 0, 0, 0};
  for (int i = 0; i < N; i ++) {
    for (int ch = 0; ch < CHANNELS; ch ++) {
      val[ch] += analogRead(ADPIN[ch]);      
    }
  }
  // N回の平均値を出力
  for (int ch = 0; ch < CHANNELS; ch ++) {
    ZeroFillPrint(val[ch] / N, 4);
    Serial.print(",");
  }
  Serial.println("");
}

