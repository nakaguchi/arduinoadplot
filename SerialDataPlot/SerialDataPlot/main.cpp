// 
// Serial Data Plot on USB Camera Background
// 2017.06.19 T.Nakaguchi
// 
// シリアル通信のプロトコル（修正可能）
// BAUDRATE 115200bps　パリティなし　データビット8bit　ストップビット1bit
// 
// シリアルデータの形式（学生）1行1サンプル
// AAAAAAAA,BBBB,CCCC,DDDD,EEEE,FFFF,GGGG\n
// A: データの通し番号 16進数8桁，データ欠損チェックに使う
// B: 入力データチャネル1 10進数4桁
// C: 入力データチャネル2 10進数4桁
// D: 入力データチャネル3 10進数4桁
// E: 入力データチャネル4 10進数4桁
// F: 入力データチャネル5 10進数4桁
// G: 入力データチャネル6 10進数4桁
//

#define _CRT_SECURE_NO_WARNINGS 1

#include "GRS232.h"
#include "myOpenCV3.h"
#include <stdlib.h>

char* COM_CONFIG = "baud=115200 parity=N data=8 stop=1";
const String WINDOW = "Serial Data Plot";	// ウインドウ名
const int ADCH = 6;	// 入力データのチャネル数
const int ADVALUEMAX = 1023;	// 入力データの最大値
const Scalar COLOR[ADCH] = { CV_RGB(255,0,0),CV_RGB(0,255,0), CV_RGB(0,0,255),
	CV_RGB(255,255,0), CV_RGB(255,0,255), CV_RGB(0,255,255) };
const int SPS_COUNT = 100;

void ErrorExit(char *msg);

int main()
{
	// シリアル接続
	int port = 4;	// デバッグモードで使うポート番号（開発中は固定する）
#ifndef _DEBUG
	std::cout << "Seirial port number = ";
	std::cin >> port;
#endif
	ComPort com;
	if (!com.Open(port, COM_CONFIG)) ErrorExit("シリアルポート接続に失敗しました\n");

	// カメラ接続
	VideoCapture cam(0);
	if (!cam.isOpened()) {
		cam.release();
		ErrorExit("カメラの接続に失敗しました\n");
	}
	Mat img;
	cam >> img;
	int width = img.size().width;
	int height = img.size().height;
	float hScale = (float)height / ADVALUEMAX;

	// ウインドウ生成
	namedWindow(WINDOW, CV_WINDOW_AUTOSIZE);

	// プロット用メモリ確保
	int *data[ADCH];
	for (int ch = 0; ch < ADCH; ch ++) data[ch]	= new int[width];

	// メインループ
	int x = 0;	// プロットする水平座標
	String buf;
	int64 lastTick = cv::getTickCount();
	double sps = 0;
	int spsCount = 0;
	while (1) {
		// カメラ撮影画像取得
		Mat plot;
		cam >> plot;

		// シリアルデータ入力
		while (1) {
			const int RAWBUFSIZE = 1024;
			char rawbuf[RAWBUFSIZE + 1];	// 終端文字分を追加
			rawbuf[RAWBUFSIZE] = '\0';
			int readSize = com.Receive((BYTE*)rawbuf, RAWBUFSIZE);
			if (readSize < 1) break;
			buf += rawbuf;
		}

		// 受信データ解読
		size_t posHead = 0;
		while (1) {
			size_t posCr = buf.find('\n', posHead);
			if (posCr == String::npos) break;
			String line = buf.substr(posHead, posCr - posHead);
			std::cout << line << std::endl;
			if (line.length() == 40) {
				for (int ch = 0; ch < ADCH; ch++) {
					data[ch][x] = std::stoi(line.substr(9 + ch * 5, 4));
				}
				if (++x >= width) x = 0;

				// サンプリングレート計算
				if (++spsCount >= SPS_COUNT) {
					int64 tick = cv::getTickCount();
					sps = cv::getTickFrequency() * SPS_COUNT / (tick - lastTick);
					lastTick = tick;
					spsCount = 0;
				}
			}
			posHead = posCr + 1;
		}
		buf = buf.substr(posHead);

		// データプロット
		for (int ch = 0; ch < ADCH; ch++) {
			for (int i = 1; i < x; i++) {
				line(plot, Point(i-1, height - (int)(data[ch][i-1] * hScale)), 
					Point(i, height - (int)(data[ch][i] * hScale)), COLOR[ch], 2);
			}
			putText(plot, format("A%d:%d", ch, data[ch][x-1]),
				Point(x - 25, height - (int)(data[ch][x - 1] * hScale) - 3), CV_FONT_HERSHEY_PLAIN, 1.0, COLOR[ch]);
		}

		// 画面更新
		putText(plot, format("%.2f sps", sps),
			Point(10, 20), CV_FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255, 255, 255));
		putText(plot, format("%.2f sps", sps),
			Point(11, 21), CV_FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 0, 0));
		cv::imshow(WINDOW, plot);
		if (waitKey(1) == 27) break;
	}

	// 終了処理
	destroyWindow(WINDOW);
	for (int ch = 0; ch < ADCH; ch++) delete[] data[ch];
	com.Close();
	return 0;
}

void ErrorExit(char *msg)
{
	fprintf(stderr, msg);
	fprintf(stderr, "Press enter to exit.");
	std::cin.ignore(std::cin.rdbuf()->in_avail());
	std::cin.get();
	exit(1);
}