#pragma once

#include <Windows.h>
#include <tchar.h>

class ComPort
{
	HANDLE mComHandle;

public:

	ComPort(void);
	~ComPort(void);
	int Open(TCHAR* config);
	bool Open(int port, TCHAR* config);
	void Close();
	DWORD Send(const BYTE* data, DWORD dataLen);
	DWORD Receive(BYTE* buffer, DWORD bufferLen);
	DWORD WaitReceive(BYTE* buffer, DWORD bufferLen, int timeout);
};