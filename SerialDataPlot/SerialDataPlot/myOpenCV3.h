
// OpenCV 3系 共通ヘッダーファイル
// T.Nakaguchi

#pragma once

// ヘッダーファイル
#pragma warning(disable: 4819)
#include <opencv2/opencv.hpp>
#pragma warning(default: 4819)

// バージョン取得
#define CV_VERSION_STR CVAUX_STR(CV_MAJOR_VERSION) CVAUX_STR(CV_MINOR_VERSION) CVAUX_STR(CV_SUBMINOR_VERSION)

// ビルドモード
#ifdef _DEBUG
#define CV_EXT_STR "d.lib"
#else
#define CV_EXT_STR ".lib"
#endif

// ライブラリのリンク（不要な物はコメントアウト）
#define PRE_COMPILE		0	// インストール版なら 1 個別ライブラリ使用時は 0
#define PREHEAD "opencv_"

#if PRE_COMPILE
// OpenCV3.0 インストール版
#pragma comment(lib, PREHEAD "world" CV_VERSION_STR CV_EXT_STR)  // 全て
#pragma comment(lib, PREHEAD "ts" CV_VERSION_STR CV_EXT_STR) // 映像関連

#else
// 個別のライブラリ指定 （CmakeやStatic版使用時など）
// 基本モジュール
#pragma comment(lib, PREHEAD "core" CV_VERSION_STR CV_EXT_STR) // 基本機能
#pragma comment(lib, PREHEAD "imgproc" CV_VERSION_STR CV_EXT_STR) // 画像処理
#pragma comment(lib, PREHEAD "imgcodecs" CV_VERSION_STR CV_EXT_STR) // 画像ファイル入出力
#pragma comment(lib, PREHEAD "videoio" CV_VERSION_STR CV_EXT_STR) // 動画ファイル入出力
#pragma comment(lib, PREHEAD "highgui" CV_VERSION_STR CV_EXT_STR) // 高機能GUI
//#pragma comment(lib, PREHEAD "video" CV_VERSION_STR CV_EXT_STR) // 動画像解析
//#pragma comment(lib, PREHEAD "calib3d" CV_VERSION_STR CV_EXT_STR) // カメラ校正と三次元再構築
//#pragma comment(lib, PREHEAD "features2d" CV_VERSION_STR CV_EXT_STR) // 画像特徴解析
//#pragma comment(lib, PREHEAD "objdetect" CV_VERSION_STR CV_EXT_STR) // 物体検出
//#pragma comment(lib, PREHEAD "ml" CV_VERSION_STR CV_EXT_STR) // 機械学習
//#pragma comment(lib, PREHEAD "flann" CV_VERSION_STR CV_EXT_STR) // 多次元クラスタリングと検索
//#pragma comment(lib, PREHEAD "photo" CV_VERSION_STR CV_EXT_STR) // 計算機写真
//#pragma comment(lib, PREHEAD "stitching" CV_VERSION_STR CV_EXT_STR) // 画像接続
//#pragma comment(lib, PREHEAD "hal" CV_VERSION_STR CV_EXT_STR) // ハードウェア高速化
//#pragma comment(lib, PREHEAD "shape" CV_VERSION_STR CV_EXT_STR) // 形状一致検出
//#pragma comment(lib, PREHEAD "superres" CV_VERSION_STR CV_EXT_STR) // 超解像
//#pragma comment(lib, PREHEAD "videostab" CV_VERSION_STR CV_EXT_STR) // 動画像安定化
//#pragma comment(lib, PREHEAD "vis" CV_VERSION_STR CV_EXT_STR) // 3次元可視化
// 拡張モジュール
//#pragma comment(lib, PREHEAD "adas" CV_VERSION_STR CV_EXT_STR) // 先進的デバイスサポート
//#pragma comment(lib, PREHEAD "aruco" CV_VERSION_STR CV_EXT_STR) // AR用マーカー
//#pragma comment(lib, PREHEAD "bgsegm" CV_VERSION_STR CV_EXT_STR) // 改良型背景・前景識別
//#pragma comment(lib, PREHEAD "bioinspired" CV_VERSION_STR CV_EXT_STR) // 生体に基づく視覚的処理
//#pragma comment(lib, PREHEAD "ccalib" CV_VERSION_STR CV_EXT_STR) // カスタムパターンによるカメラ校正と三次元再構成
//#pragma comment(lib, PREHEAD "cvv" CV_VERSION_STR CV_EXT_STR) // 対話的視覚的デバッグGUI
//#pragma comment(lib, PREHEAD "datasets" CV_VERSION_STR CV_EXT_STR) // 多種データセット取り扱いフレームワーク
//#pragma comment(lib, PREHEAD "face" CV_VERSION_STR CV_EXT_STR) // 顔認識
//#pragma comment(lib, PREHEAD "latentsvm" CV_VERSION_STR CV_EXT_STR) // Latent-SVM
//#pragma comment(lib, PREHEAD "line_descriptor" CV_VERSION_STR CV_EXT_STR) // 線検出のバイナリ表現
//#pragma comment(lib, PREHEAD "matlab" CV_VERSION_STR CV_EXT_STR) // MATLABブリッジ
//#pragma comment(lib, PREHEAD "optflow" CV_VERSION_STR CV_EXT_STR) // オプティカルフロー
//#pragma comment(lib, PREHEAD "reg" CV_VERSION_STR CV_EXT_STR) // 画像位置合わせ
//#pragma comment(lib, PREHEAD "rgbd" CV_VERSION_STR CV_EXT_STR) // RGB-深度カメラ
//#pragma comment(lib, PREHEAD "saliency" CV_VERSION_STR CV_EXT_STR) // 画像 顕著性 API
//#pragma comment(lib, PREHEAD "surface_matching" CV_VERSION_STR CV_EXT_STR) // 表面モデル一致検出
//#pragma comment(lib, PREHEAD "text" CV_VERSION_STR CV_EXT_STR) // シーン文字検出と認識
//#pragma comment(lib, PREHEAD "tracking" CV_VERSION_STR CV_EXT_STR) // 追跡
//#pragma comment(lib, PREHEAD "xfeatures2d" CV_VERSION_STR CV_EXT_STR) // 拡張版 画像特徴解析
//#pragma comment(lib, PREHEAD "ximgproc" CV_VERSION_STR CV_EXT_STR) // 拡張版 画像処理
//#pragma comment(lib, PREHEAD "xobjdetect" CV_VERSION_STR CV_EXT_STR) // 拡張版 物体検出
//#pragma comment(lib, PREHEAD "xphoto" CV_VERSION_STR CV_EXT_STR) // 拡張版 計算機写真
#endif

using namespace cv;
